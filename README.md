# OpenML dataset: kin8nm

https://www.openml.org/d/189

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown -   
**Please cite**:   

This is data set is concerned with the forward kinematics of an 8 link
 robot arm. Among the existing variants of this data set we have used
 the variant 8nm, which is known to be highly non-linear and medium
 noisy.

 Original source: DELVE repository of data. 
 Source: collection of regression datasets by Luis Torgo (ltorgo@ncc.up.pt) at
 http://www.ncc.up.pt/~ltorgo/Regression/DataSets.html
 Characteristics: 8192 cases, 9 attributes (0 nominal, 9 continuous).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/189) of an [OpenML dataset](https://www.openml.org/d/189). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/189/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/189/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/189/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

